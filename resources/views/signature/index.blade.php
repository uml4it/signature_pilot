@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class=" col-sm-12  col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Signatures</div>

                <div class="panel-body">
                   @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    
                    
                    
                    
                
                     
                         @if($signatures)

                            <div class="row">
                             @foreach($signatures as $signature)
                             
                                
                                                <div class="col-sm-6 col-md-4">
                                                  <div class="thumbnail">
                                                    <a href="#"  data-toggle="modal" data-target="#lightbox"> 
                                                            <img class="img-rounded signature-img-saved"    src="{{$signature->signature ? asset( $signature->signature ): 'http://placehold.it/400x400'}}" alt="Click to view" >
                                                        </a>  
                                                    <div class="caption">
                                                      <h3>Created at : {{$signature->created_at->diffForhumans()}}</h3>
                                                      <p>...</p>
                                                      <p><form method="post" action="{{ route('signature.destroy',$signature->id) }}">
                                                            {{ method_field('DELETE') }}
                                                            <div class="form-group">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <input type="submit" class="btn btn-primary" value="Delete"  Onclick="return confirmDelete()"/>

                                                      </form>  <a class="btn btn-success" href="{{ route('signature.edit',$signature->id) }}">Edit</a> </p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                           
                                    
                            

                             @endforeach
  </div>
                         
                         <div class="row">
                            <div class="col-lg-6 col-sm-offset-5">
                                {{ $signatures->render() }}
                            </div>

                        </div>
                             @endif

                        
                <div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
