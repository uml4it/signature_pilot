@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class=" col-sm-12  col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                   @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    <div id="signature-pad" class="signature-pad">
                        <div class="wrapper" id="">
<!--                               <img src="http://www.licensing.biz/cimages/c0f1a3a97bc0de97271f9813f7715bb3.png" width=450 height=350 />-->
                                <canvas id="signature-pad-im"  width=650 height=350  ></canvas>
                        </div>
                        <div class="signature-pad--footer">
                          <div class="description">
                              <img  id="last-signature-img" style="display: none;" src="{{ asset( $signature->signature ) }}">
                          
                          
                          </div>

                          <div class="signature-pad--actions">
                            <div>
                              <button type="button" id="clear-sg" class="button clear" data-action="clear">Clear</button>                              
<!--                              <button type="button" id="under-sg" class="button" data-action="undo">Undo</button>-->

                            </div>
                            <div>
                                <button type="button" id="save-signature" class="btn btn-primary" >Save </button>
                                <a class="btn btn-default" href="{{ route('signature.index') }}">Back</a>
                            </div>
                          </div>
                        </div>
                      </div>
<!--                    <textarea id='output'></textarea><br/>
                     Preview image 
                    <img crossOrigin="Anonymous" src='' id='sign_prev' style='display: none;' />-->
                    <form method="POST" id="myform" action="{{  route('signature.update',$signature->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <input type='hidden' id="input_signature" name='signature' value='' />
                    </form>
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    
    jQuery(document).ready(function () {
        
   
        var canvas = document.getElementById('signature-pad-im');
         var signaturePad = new SignaturePad(document.getElementById('signature-pad-im'), {
			  backgroundColor: 'rgba(255, 255, 255, 0)',
			  penColor: 'rgb(0, 0, 0)'
			});
          context = canvas.getContext('2d');
           signaturePad.clear();
            base_image = new Image();

            base_image.src = $('#last-signature-img').attr('src');
            
        // alert($('#signature-pad-im').width());
          var canvas_width =$('#signature-pad-im').width();
          var canvas_height =$('#signature-pad-im').height();
            base_image.height  =canvas_height  ;
            base_image.width  =  canvas_width;
            //base_image.crossOrigin = "Anonymous";
            base_image.onload = function(){
            //  context.drawImage(base_image, 0, 0,canvas.width,canvas.height);

                    context.drawImage(base_image, 0, 0,canvas_width,canvas_height);
      
            }
         
        
        
    })
    
    
</script>


@endsection

