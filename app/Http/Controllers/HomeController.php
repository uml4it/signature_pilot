<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Signature;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    
    public function submitSignature(Request $request)
    {
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $sub_folder = $year.'/'.$month.'/'.$day.'/';
        $upload_url= 'images/'.$sub_folder;

        if (! File::exists(public_path().'/'.$upload_url)) {
            File::makeDirectory(public_path().'/'.$upload_url,0777,true);
       }

        $signatureName = time() . '.' . "png";  
        $signature = $request->signature;
        $encoded_image = explode(",", $signature)[1];
        $decoded_image = base64_decode($encoded_image);
        Storage::disk("images")->put($sub_folder.$signatureName, $decoded_image);
        
        $signature = new Signature();       
        $signature->signature =  'images/'.$sub_folder.$signatureName;
        $signature->save();
        
        return redirect()->back()->with('success', 'The signature has been saved successfully.');
    }
    
}
