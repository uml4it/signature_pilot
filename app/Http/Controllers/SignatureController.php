<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Signature;

class SignatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $signatures = Signature::orderBy('created_at', 'desc')->paginate(10);
        
         return view('signature.index', ['signatures'=>$signatures]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $signature = Signature::findOrFail($id);
        
         return view('signature.edit', ['signature'=>$signature]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $signature = Signature::findOrFail($id);
         
         // delete the old one
         if (is_file(public_path() . $signature->signature)) {

            unlink(public_path() . $signature->signature);
        }
        
        
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $sub_folder = $year.'/'.$month.'/'.$day.'/';
        $upload_url= 'images/'.$sub_folder;

        if (! File::exists(public_path().'/'.$upload_url)) {
            File::makeDirectory(public_path().'/'.$upload_url,0777,true);
       }

        $signatureName = time() . '.' . "png";  
        $signature_img = $request->signature;
        $encoded_image = explode(",", $signature_img)[1];
        $decoded_image = base64_decode($encoded_image);
        Storage::disk("images")->put($sub_folder.$signatureName, $decoded_image);
         
        $signature->signature =  'images/'.$sub_folder.$signatureName;
        $signature->save();
        
        return redirect()->route('signature.index')->with('success', 'The signature has been updated successfully.');
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $signature = Signature::findOrFail($id);
         
         if (is_file(public_path() . $signature->signature)) {

            unlink(public_path() . $signature->signature);
        }
         

         $signature->delete();
         
         return redirect()->back()->with('success', 'The signature has been deleted successfully.');
    }
}
